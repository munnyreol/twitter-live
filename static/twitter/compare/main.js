var Twitter = {};
var startRefresh;

window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());
gtag('config', 'UA-119417406-7');

Twitter.updateManager = {
    updateAvatar: function (a, b) {
        document.querySelector(".profile-picture-1").src = a;
        document.querySelector(".profile-picture-2").src = b;
    },
    updateBanner: function (a, b) {
        document.querySelector(".banner-1").src = a
        document.querySelector(".banner-2").src = b
    },
    updateUsername: function (a, b) {
        let items1 = document.querySelectorAll(".username-1");
        let items2 = document.querySelectorAll(".username-2");

        for (let i = 0; i < items1.length; i++) {
            items1[i].innerHTML = a
        }

        for (let i = 0; i < items2.length; i++) {
            items2[i].innerHTML = b
        }

        document.querySelector(".social-button-1").href = "https://twitter.com/" + user1
        document.querySelector(".social-button-2").href = "https://twitter.com/" + user2
    },
    updateFollowerCount: function (a, b) {
        document.querySelector(".main-odometer-1").innerHTML = a;
        document.querySelector(".main-odometer-2").innerHTML = b;

        //Update Difference
        document.querySelector(".difference-odometer").innerHTML = a - b
    }
}

Twitter.refreshManager = {
    start: function () {
        startRefresh = setInterval(function () {
            $.getJSON('https://cors-munnyreol.herokuapp.com/http://munnyreol.herokuapp.com/twitter/api/shaz/?user=' + user1, function (data1) {
                $.getJSON('https://cors-munnyreol.herokuapp.com/http://munnyreol.herokuapp.com/twitter/api/shaz/?user=' + user2, function (data2) {
                    Twitter.updateManager.updateFollowerCount(data1.followers, data2.followers)
                })
            })
        }, 3000)
    },
    stop: function () {
        clearInterval(startRefresh)
    }
}

// ---------------------------------- //

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value
    });
    return vars
}

function getData() {
    $.getJSON('https://cors-munnyreol.herokuapp.com/http://munnyreol.herokuapp.com/twitter/api/shaz/?user=' + user1, function (data1) {
        $.getJSON('https://cors-munnyreol.herokuapp.com/http://munnyreol.herokuapp.com/twitter/api/shaz/?user=' + user2, function (data2) {
            Twitter.updateManager.updateAvatar(data1.avatar, data2.avatar)
            Twitter.updateManager.updateBanner(data1.banner, data2.banner)
            Twitter.updateManager.updateUsername(data1.fullUsername, data2.fullUsername)
            Twitter.updateManager.updateFollowerCount(data1.followers, data2.followers)
        }).fail(function () {
            setTimeout(function () {
                getData();
            }, 1000)
        })
    }).fail(function () {
        setTimeout(function () {
            getData();
        }, 1000)
    })
}

function searchUser1() {
    var change_user = document.querySelector(".change-user-1-search").value
    if (change_user.length == 0) {
        alert("Invalid Username!")
    } else {
        window.location.href = "/twitter-realtime/compare/" + change_user + "/" + user2
    }
}

function searchUser2() {
    var change_user = document.querySelector(".change-user-2-search").value
    if (change_user.length == 0) {
        alert("Invalid Username!")
    } else {
        window.location.href = "/twitter-realtime/compare/" + user1 + "/" + change_user
    }
}


// ---------------------------------- //

setTimeout(function () {
    getData();
    Twitter.refreshManager.start();
}, 1)

// ---------------------------------- //

// Change User Handler

$(".change-user-search-button-1").click(function () {
    searchUser1();
})

$(".change-user-1-search").keyup(function (event) {
    if (event.keyCode === 13) {
        searchUser1()
    }
});

// Compare User Handler

$(".change-user-search-button-2").click(function () {
    searchUser2();
})

$(".change-user-2-search").keyup(function (event) {
    if (event.keyCode === 13) {
        searchUser2();
    }
});


var disqus_config = function () {
    this.page.url = 'https://livecounts.io/Twitter-realtime/compare/?user1=' + user1 + '&user2=' + user2;
};

(function () {
    var d = document,
        s = d.createElement('script');
    s.src = 'https://munny-reol.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
})();
